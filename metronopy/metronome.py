#!/usr/bin/env python3
# -*- coding=utf-8 -*-
"""
Play a metronome

Start a metronome from the command line
Arguments:
    bpm: Beats per minute
    pitch: Pitch of beat noise

This can be done in a couple ways

    Running through python:
        python metronome.py <ADD ARGUMENTS HERE>

    Running as script:
        chmod +x metronome.py # This only needs to be run once. It gives you permission to do the next step
        ./metronome.py <ADD ARGUMENTS HERE>

## License
Copyright 2022 Paul Saliminen <paulsalminen@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# Standard library
# Used to do time-related stuff
import os
import sys
import time

# Used to handle arguments given to the program from the command line
# See the documentation below for usage
# https://docs.python.org/3/library/argparse.html
# import argparse

# 3rd party sound-related packages
import librosa
import sounddevice as sd


def play_metronome(bpm: int = 120, pitch: int = 440) -> None:
    """Play the metronome
    + bpm -> Desired beats per minute. Default is 120 bpm
    + pitch -> Pitch of beep sound. Default is 440hz (aka Middle A)
    """
    screen_length = os.get_terminal_size().columns
    length = int(screen_length / 2)
    on = "#"
    off = " "
    full = False

    delay = 60 / bpm
    sample_rate = 22050
    tone = librosa.tone(pitch, sr=sample_rate, length=1000)

    last_time = time.perf_counter()
    current_time = time.perf_counter()
    while True:
        current_time = time.perf_counter()
        delta = abs(last_time - current_time)
        time.sleep(delay / 2.0)

        while True:
            current_time = time.perf_counter()
            if current_time - last_time >= delay:
                sys.stdout.flush()
                sys.stdout.write("\r")
                # the exact output you're looking for:
                if full:
                    sys.stdout.write("%s%s" % (on * length, off * length))
                    full = False
                else:
                    sys.stdout.write("%s%s" % (off * length, on * length))
                    # sys.stdout.write("%s" % (' '*100))
                    full = True
                sd.play(tone, sample_rate)
                last_time = current_time
                break


def main() -> None:
    """Main function that runs"""
    # Use argparse to define
    try:
        bpm, pitch = sys.argv[1:]
    except ValueError as _:
        bpm, pitch = 120, 440
    bpm, pitch = int(bpm), int(pitch)
    # This try/except stuff is kinda advanced
    # Pretty much, it will run play_metronome
    # Until there is a keyboard interrupt signal (ie. ctrl-C)
    try:
        play_metronome(bpm, pitch)
    except KeyboardInterrupt:
        print("Metronome has stopped")


if __name__ == "__main__":
    main()
